Source: fcitx5-table-other
Section: utils
Priority: optional
Maintainer: Lu YaNing <dluyaning@gmail.com>
Build-Depends: cmake,
 debhelper-compat (= 13),
 extra-cmake-modules,
 libboost-dev,
 libfcitx5utils-dev,
 libimecore-dev,
 libimetable-dev,
 libime-bin,
Standards-Version: 4.5.1
Homepage: https://github.com/fcitx/fcitx5-table-other
Vcs-Git: https://salsa.debian.org/Dami/fcitx5-table-other.git
Vcs-Browser: https://salsa.debian.org/Dami/fcitx5-table-other
Rules-Requires-Root: no

Package: fcitx5-table
Architecture: all
Depends:
 ${misc:Depends},
Description: Flexible Input Method Framework
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides metadata.

Package: fcitx5-table-amharic
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Amharic table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Amharic table used by the Fcitx table engine.

Package: fcitx5-table-arabic
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Arabic table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Arabic table used by the Fcitx table engine.

Package: fcitx5-table-cns11643
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Cns11643 table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Cns11643 table used by the Fcitx table engine.

Package: fcitx5-table-compose
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Compose table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Compose table used by the Fcitx table engine.

Package: fcitx5-table-emoji
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Emoji table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Emoji table used by the Fcitx table engine.

Package: fcitx5-table-ipa-x-sampa
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - IPA-X-SAMPA table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides IPA-X-SAMPA table used by the Fcitx table engine.

Package: fcitx5-table-latex
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - LaTeX table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides LaTeX table used by the Fcitx table engine.

Package: fcitx5-table-malayalam-phonetic
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Malayalam phonetic table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Malayalam phonetic table used by the Fcitx table engine.

Package: fcitx5-table-rustrad
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Rustrad table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Rustrad table used by the Fcitx table engine.

Package: fcitx5-table-tamil-remington
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Tamil Remington table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Tamil Remington table used by the Fcitx table engine.

Package: fcitx5-table-thai
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Thai table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Thai table used by the Fcitx table engine.

Package: fcitx5-table-translit
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Translit table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Translit table used by the Fcitx table engine.

Package: fcitx5-table-translit-ua
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Ukrainian Translit table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Ukrainian Translit table used by the Fcitx table
 engine.

Package: fcitx5-table-viqr
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Viqr table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Viqr table used by the Fcitx table engine.

Package: fcitx5-table-yawerty
Architecture: all
Depends:
 fcitx5-table (= ${binary:Version}),
 ${misc:Depends},
Description: Flexible Input Method Framework - Yawerty table
 Fcitx is a input method framework with extension support, which provides
 an interface for entering characters of different scripts in applications
 using a variety of mapping systems.
 .
 It offers a pleasant and modern experience, with intuitive graphical
 configuration tools and customizable skins and mapping tables. It is
 highly modularized and extensible, with GTK+ 2/3 and Qt4 IM Modules, support
 for UIs based on Fbterm, pure Xlib, GTK+, or KDE, and a developer-friendly
 API.
 .
 This package provides Yawerty table used by the Fcitx table engine.
